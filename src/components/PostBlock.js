import React from 'react';
import './PostBlock.css';
import ReactTimeAgo from 'react-time-ago';
import { BrowserRouter as Router, Link } from 'react-router-dom';

const PostBlock = (props) => {
  const { title, createdDate, author, thumbnail, comments, dismissClick, id } = props;

  const formatedDate = new Date(createdDate);
  const formatedTitle = limitTitle(title);
  const link = `/post/${id}`;

  return (
    <article className="post-block">
      <Router>
        <Link to={link}>
          <header>
            <span className="post-block-author">{author}</span>
            <span className="post-block-date"><ReactTimeAgo date={formatedDate}/></span>
          </header>
        </Link>
      </Router>
      <main>
        <img className="post-block-image" src={thumbnail} alt={formatedTitle}/>
        <span className="post-block-title" >{formatedTitle}</span>
      </main>
      <footer>
        <button className="post-block-dismiss" onClick={() => { dismissClick(id)} }>
          Dismiss post
        </button>
        <span className="post-block-comments">
          {comments} Comments
        </span>
      </footer>
    </article>
  );
}

function limitTitle(title, customLimit) {
  let limit = 100;
  if (customLimit) {
    limit = customLimit;
  }

  let formatedTitle = title;
  if (title.length > limit) {
    formatedTitle = `${title.substring(0, limit)}...`;
  }

  return formatedTitle;
}

export default PostBlock;