import React from 'react';
import './DetailBlock.css';

const DetailBlock = (props) => {
  const { detail } = props;

  return (
    <div className='detail-block'>
      <div className='detail-block-author'>{detail.author}</div>
      <div className='detail-block-image'>
        <img src={detail.thumbnail} alt={detail.title} />
      </div>
      <div className='detail-block-title'>{detail.title}</div>
    </div>
  );
}

export default DetailBlock;