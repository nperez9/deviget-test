import React from 'react';
import PostList from './containers/PostList';
import Detail from './containers/Detail';
import { BrowserRouter as Router, Route } from 'react-router-dom';


function App() {
  return (
    <main className="App">
      <PostList></PostList>
      <Router>
        <Route path="/post/:id" component={Detail} />
      </Router>
    </main>
  );
}

export default App;
