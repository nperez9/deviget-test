import React, { Component } from 'react';
import './PostList.css';
import PostBlock from '../components/PostBlock';

import { listMock } from '../_mocks_/list.mock';
const list = listMock;

class PostList extends Component {
  state = {
    posts: []
  };

  componentDidMount = () => {
    const posts = list.data.children;
    this.setState({posts});
  }

  dismissPostHandler = (id) => {
    const posts = this.state.posts.filter(p => p.data.id !== id);
    this.setState({posts});
  }

  dismissAllHandler = () => {
    this.setState({posts: []});
  }

  render() {
    let posts = null;
    if (this.state.posts.length > 0) {
      posts = this.state.posts.map((post) => {
        return(
          <PostBlock
            key={post.data.id}
            id={post.data.id}
            title={post.data.title}
            author={post.data.author}
            thumbnail={post.data.thumbnail}
            comments={post.data.num_comments}
            visited={post.data.visited}
            createdDate={post.data.created_utc}
            dismissClick={this.dismissPostHandler}
          ></PostBlock>
        );
      });
    }

    return (
      <aside className='post-list'>
        <div className='post-list-title'>
          <h1>Reddit Post</h1>
        </div>
        <div className='post-list-blocks'>
          {posts}
        </div>
        <div className='post-list-dismiss'>
          <button onClick={this.dismissAllHandler}>
            Dismiss All
          </button>
        </div>
      </aside>
    )
  }

}

export default PostList;