import React, { Component } from 'react';
import './Detail.css';
import { listMock } from '../_mocks_/list.mock';
import DetailBlock from '../components/DetailBlock';
const list = listMock;

class Detail extends Component {
  state = {
    detail: null,
    id: null,
  };

  componentDidMount = () => {
    const id = this.props.match.params.id;
    this.getPostDetail(id);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log('quien te conoce?');
    if (nextProps.match.params.id) {
      return { id: nextProps.match.params.id };
    }
    else {
      return null;
    }
  }
  
  componentDidUpdate(prevProps, prevState) {
    console.log('papa');
    if (prevProps.match.params.id !== this.state.id) {
      this.getPostDetail(this.state.id);
    }
  }

  getPostDetail(id) {
    if (!id) {
      console.log('aca pase');
      return false;
    }
    const detail = list.data.children.find(l => l.data.id === id);
    console.log(detail);
    this.setState({detail: detail.data});
  }

  render() {
    let detail = null;

    if (this.state.detail) {
      detail = (
        <DetailBlock
          detail={this.state.detail}
        ></DetailBlock>
      );
    }
     
    return (
      <div className='detail'>
        {detail}
      </div>
    );
  }
}

export default Detail;